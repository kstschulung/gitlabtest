FROM node:16-alpine

ENV NODE_ENV = prod

COPY . /home/node
WORKDIR /home/node

RUN chown -R node:node /home/node

USER node
RUN npm install --only=prod

EXPOSE 8080
CMD ["node", "index"]
