'use strict';

const Hapi = require('@hapi/hapi');

const ENV = process.env;
const PORT = ENV.PORT || 8080;

(async () => {
  const server = Hapi.server({ port: PORT, });
  server.route({
    method: 'GET',
    path: '/',
    handler: async (request, h) => {
      return 'Hallo Welt';
    }
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
})();

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});
